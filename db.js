let mysql = require('mysql');
const config = require("./config.json").sqlConfig;
let connection = mysql.createConnection(config);

const bcrypt = require('bcrypt');

connection.connect(function (err) {
    if (err) throw err;
    console.log("Connected!");
});

// Kampen en al
function getCamps(cb) {
    connection.query("select camps.id, title, startTime, endTime, image, campGroup.capacity, COUNT(registration.id) as members, camps.`date`  from camps left join campGroup on camps.id  = campGroup.campId left join registration  on campGroup.campId = registration.`group` GROUP BY camps.id" ,
        (err, result) => {
            if (err) {
                return cb(err);
            } else {
                return cb(err, result);
            }

        });
}

function getRegistrationByGroup(id, cb) {
    connection.query("select COUNT(id) as members FROM registration WHERE `group` = ?",
    [id],
        (err, result) => {
            if (err) {
                return cb(err);
            } else {
                return cb(err, result);
            }

        });
}

function getlevelByGroup(id, cb) {
    connection.query("select * FROM camps  INNER JOIN campGroup  on camps.id  = campGroup.campId INNER JOIN `level` l on campGroup.`level` = l.id  where camps.id  = ?",
        [id],
        (err, result) => {
            if (err) {
                console.log(err)
                return cb(err);
            } else {
                return cb(err, result);
            }

        });
}


// Login en register
function registerUser(email, password, cb) {
    console.log(email, password);
    bcrypt.hash(password, 10).then(function (hashedPassword) {
        connection.query("insert into users (email, password) values (?,?)", [email, hashedPassword], (err, res) => {
            if (err) throw err;
            cb({
                "message": "Registered"
            });
        })
    });

}

function getUserByEmail(email, cb) {
    let user = {};
    connection.query("select * from users where email == ?", [email], (err, rows) => {
        if (err) throw err;
        user = rows[0];
        cb(user);
    })


}

function getMembersByLevel(id, level, cb) {
    connection.query("SELECT COUNT(registration.id) as registrations, campGroup.capacity from campGroup left join registration  on campGroup.`level`  = registration.`grouplevel`  and campGroup.campId = registration.`group` WHERE  campId  = ? AND campGroup.`level` = ?",
        [id, level],
        (err, result) => {
            if (err) {
                return cb(err);
            } else {
                return cb(err, result);
            }
        });
}

function insertMember(groupid, levelid, name, cb) {
    connection.query("INSERT INTO registration (childName, grouplevel, `group`) VALUES (?, ?, ?)",
        [name, levelid, groupid],
        (err, result) => {
            if (err) {
                return cb(err);
            } else {
                return cb(err, result);
            }
        });
}

module.exports = {
    getCamps,
    getlevelByGroup,
    getMembersByLevel,
    insertMember,
    registerUser,
    getUserByEmail,
    getRegistrationByGroup,
};