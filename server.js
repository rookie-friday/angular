const express = require('express');
const app = express();
const port = 3000;
const repo = require("./db.js");
var cors = require('cors');

app.use(cors());
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send('Boo!');
});

app.get('/api/camps', cors(), (req, res) => {
  repo.getCamps((err,result) => {
    res.send(result);
  });
  /* let membersarray = [];
  let final = [];
  let camps = [];
  repo.getCamps((err, result)=> {
    camps = JSON.parse(JSON.stringify(result));
    camps.forEach(element => {
    repo.getRegistrationByGroup(element.id, (err, members) => {
      
        console.log(members)
        console.log(JSON.parse(JSON.stringify(members)));
        membersarray.push(JSON.parse(JSON.stringify(members))[0]);
        console.log('Robbe heeft gepushed, ', membersarray) 
      });
    });
    console.log("YOLO COD FOR LIFE", membersarray);
    res.json({"test": camps, "testje": membersarray});
  }); */
});

app.get('/api/camps/:id', cors(), (req, res) => {
  console.log(req.params.id);
  repo.getlevelByGroup(req.params.id, (err, result)=> {
      res.json({ result: result});
  });
});


// Log in en register
app.post('/api/register', cors(), bodyParser, (req, res) => repo.registerUser(req.body.email, req.body.password, result => {
  res.send(result);
}));

// insert member
app.post('/api/camps/register',  cors(), (req, res) => {
  repo.getMembersByLevel(req.body.groupId, req.body.levelId, (err, result)=> {
    let results = JSON.parse(JSON.stringify(result));
    console.log(results);
    if (results[0].capacity <= results[0].registrations) {
      res.json({
        "success": "false",
        "message": "Deze les zit vol!"
      });
    } else {
      repo.insertMember(req.body.groupId, req.body.levelId, req.body.name, (err, result)=> {
        res.json({
          "success": "true",
          "message": "U bent ingeschreven!"
        });
      });
    }
  });
  
});



app.listen( process.env.PORT || port, () => {
  console.log(`app listening at http://localhost:${port || process.env.PORT}`);
});
