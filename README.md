# webdream backend
owner: Maarten Vansever

## installation

1. npm install
2. setup config.json

## run server
npm start

## routes

### camps

- url/camps (example: localhost:3000/camps)
- fetches all camps  with their (id, title, startTime, endTime, image, total)
- params: none

